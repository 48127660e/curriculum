# Curriculum

----
## Oriol lacuesta ramón
![Oriol](f1.png "Oriol")


**Nombre:** Oriol lacuesta  
**Direción:** Rambla pueblonuevo    
**Teléfono:** 663916141  
**Correo electrónico:** 48127660e@iespoblenou.org      

----
## Estudios
1. Educación secundaria obligatória
2. Grado medio de informática

----
## Idiomas

Idioma | comprensión oral | comprensión escrita | expresión oral | expresión escrita 
---|---|---|---|---
Catalán | Muy bueno | Muy bueno | Muy bueno | Muy bueno  
Castellano | Muy bueno | Muy bueno | Muy bueno | Muy bueno  
Inglés | Bajo | Bueno | Bajo | Bueno  


## Experiencia laboral

Prácticas en una escuela de informática durante 4 meses.


